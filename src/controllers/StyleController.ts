/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {Config, ErrorModel, HttpController, IBlueprintRoutes, mkdir, pathResolve, read, rootPathResolve, unlink, write} from 'hk-serv';

import {StyleDao} from '../dao/StyleDao';
import {StyleEntity} from '../entity/StyleEntity';

const publicPath: string = Config.get('server.publicFolder');

export class StyleController extends HttpController implements IBlueprintRoutes {

    styleDao: StyleDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.styleDao = new StyleDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'styleName',
            'code',
        ]);

        const style: StyleEntity = this.getStyleWithParams();

        const find: StyleEntity | null = await this.styleDao.getItem({styleName: style.styleName});

        if (find) {

            throw new ErrorModel('Style Defined', 400);
        }

        await this.styleDao.insert(new StyleEntity({...style}));

        await this.writeStyle(style);

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: StyleEntity | null = await this.styleDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Style not found', 404);
        }

        this.unlinkStyle(find);

        await this.styleDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {

        const _id: string = this.getParam('id');

        const style: StyleEntity | null = await this.styleDao.getItem({_id});

        if (style) {

            style.code = await this.getStyleCode(style);

            this.ok({style});
        } else {

            this.notFoundError(new ErrorModel('Style Not Found!'));
        }

    }

    async items(): Promise<void> {

        const filter: StyleEntity = this.getStyleWithParams();

        const styles: StyleEntity[] | null = await this.styleDao.getItems(filter);

        this.ok({styles});
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const style: StyleEntity = this.getStyleWithParams();
        const find: StyleEntity | null = await this.styleDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Style not found', 404);
        }

        if (style.styleName !== find.styleName) {

            style.code = style.code ? style.code : (await this.getStyleCode(find));

            await this.unlinkStyle(find);

        }

        await this.writeStyle(style);

        await this.styleDao.updateWithId(_id, new StyleEntity({
            ...style,
            _id
        }));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        this.update();
    }

    private getStyleWithParams(values: any = {}): StyleEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'styleName',
                'code',
            ])
            .getValues();
    }

    private async getStylePath(styleName: string | undefined): Promise<string> {

        const folder: string = rootPathResolve(publicPath, 'styles');

        await mkdir(folder);

        return pathResolve(publicPath, 'styles', `${styleName}.css`);
    }

    private async getStyleCode(style: StyleEntity): Promise<string> {

        const styleFile: string = await this.getStylePath(style.styleName);

        const buffer: Buffer = await read(styleFile);

        return buffer.toString();
    }

    private async writeStyle(style: StyleEntity): Promise<void> {

        const styleFile: string = await this.getStylePath(style.styleName);

        await write(styleFile, style.code || '');

    }

    private async unlinkStyle(style: StyleEntity): Promise<void> {

        const styleFile: string = await this.getStylePath(style.styleName);

        await unlink(styleFile);

    }
}
