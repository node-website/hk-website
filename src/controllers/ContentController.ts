/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';
import * as _ from 'lodash';

import {ContentDao} from '../dao/ContentDao';
import {LanguageDao} from '../dao/LanguageDao';
import {PageDao} from '../dao/PageDao';
import {ContentEntity} from '../entity/ContentEntity';
import {ImageEntity} from '../entity/ImageEntity';
import {PageEntity} from '../entity/PageEntity';

export class ContentController extends HttpController implements IBlueprintRoutes {

    pageDao: PageDao;
    languageDao: LanguageDao;
    contentDao: ContentDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.contentDao = new ContentDao(this.db);
        this.pageDao = new PageDao(this.db);
        this.languageDao = new LanguageDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'contentName',
            'title',
        ]);

        const now: number = Date.now();

        const content: ContentEntity | null = this.getContentWithParams({
            createdTime: now,
            updatedTime: now
        });

        const isFindContent: ContentEntity | null = await this.contentDao.getItem({contentName: content.contentName});

        if (isFindContent) {

            throw new ErrorModel('Content Name Defined', 400);
        }

        await this.contentDao.insert(new ContentEntity({...content}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: ContentEntity | null = await this.contentDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Content not found', 404);
        }

        await this.contentDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const content: ContentEntity | null = await this.contentDao.getItem({_id});

        if (content) {

            this.ok({content});
        } else {

            this.notFoundError(new ErrorModel('Content Not Found!'));
        }

    }

    async items(): Promise<void> {

        const filter: PageEntity = this.getContentWithParams();

        const contents: ContentEntity[] | null = await this.contentDao.getItems(filter);

        this.ok({contents});
    }

    async partialUpdate(): Promise<void> {

        this.update();
    }

    async update(): Promise<void> {

        const _id: string = this.getParam('id');

        const content: ContentEntity | null = this.getContentWithParams({
            updatedTime: Date.now()
        });

        const find: ContentEntity | null = await this.contentDao.getItem({_id});

        if (!find) {

            this.notFoundError(new ErrorModel('Content not found'));

            return;
        }

        const updateContent: ContentEntity = new ContentEntity({
            ...find,
            ...content,
            _id,
        });

        await this.contentDao.updateWithId(_id, updateContent);

        this.ok({success: 'OK'});
    }

    getContentWithParams(values: any = {}): ContentEntity {
        const content: ContentEntity = this
            .setValues(values)
            .setValuesWithParams([
                'contentName',
                'title',
                'subtitle',
                'images',
                'htmlCode',
                'json',
                'keywords',
                'connectedPageId'
            ])
            .getValues();

        if (content.images) {
            content.images = _.map(content.images, (image: any) => new ImageEntity(image));
        }

        return content;
    }
}
