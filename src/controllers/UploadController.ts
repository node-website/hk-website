/**
 * Created by hakan on 04/07/2017.
 */
import {HttpController} from 'hk-serv';

export class UploadController extends HttpController {

    /**
     * @apiVersion 1.0.0
     * @src {get} /upload File uploader
     * @apiName Upload
     * @apiGroup Upload
     * @apiDescription Dosya Yükleme işlemi sağlar
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      { success: 'OK' }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /upload
     */
    public async upload(): Promise<void> {

        await this.uploadHandler([
            {name: 'files', maxCount: 10},
        ]);

        const files: { uploaded: any[], notUploaded: any[] } = this.getFiles(['files']);

        for (const image of files.uploaded) {
            image.filename = `/uploads/${image.filename}`;
        }

        this.ok(files);

    }

}
