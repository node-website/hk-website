/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';

import {TranslateDao} from '../dao/TranslateDao';
import {TranslateEntity} from '../entity/TranslateEntity';

export class TranslateController extends HttpController implements IBlueprintRoutes {

    translateDao: TranslateDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.translateDao = new TranslateDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'langId',
            'key',
            'value',
        ]);

        const translate: TranslateEntity = this.getTranslateWithParams();

        const find: TranslateEntity | null = await this.translateDao.getItem({key: translate.key});

        if (find) {

            throw new ErrorModel('Translate Defined', 400);
        }

        await this.translateDao.insert(new TranslateEntity({...translate}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: TranslateEntity | null = await this.translateDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Translate not found', 400);
        }

        await this.translateDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const translate: TranslateEntity | null = await this.translateDao.getItem({_id});

        if (translate) {

            this.ok({translate});
        } else {

            this.notFoundError({message: 'Translate Not Found!'});
        }

    }

    async items(): Promise<void> {

        const filter: TranslateEntity = this.getTranslateWithParams();

        const translates: TranslateEntity[] | null = await this.translateDao.getItems(filter);

        this.ok({translates});
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const translate: TranslateEntity = this.getTranslateWithParams({
            updatedTime: Date.now()
        });

        await this.translateDao.updateWithId(_id, new TranslateEntity({
            ...translate,
            _id
        }));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        const _id: string = this.getParam('id');

        const translate: TranslateEntity = this.getTranslateWithParams();

        const find: TranslateEntity | null = await this.translateDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Translate not found!', 404);
        }

        await this.translateDao.updateWithId(_id, new TranslateEntity({
            ...translate,
            _id
        }));

        this.ok({success: 'OK'});
    }

    private getTranslateWithParams(values: any = {}): TranslateEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'langId',
                'key',
                'value',
            ])
            .getValues();
    }
}
