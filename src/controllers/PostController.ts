/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';

import {LanguageDao} from '../dao/LanguageDao';
import {PageDao} from '../dao/PageDao';
import {PostDao} from '../dao/PostDao';
import {PageEntity} from '../entity/PageEntity';
import {PostEntity} from '../entity/PostEntity';

export class PostController extends HttpController implements IBlueprintRoutes {

    pageDao: PageDao;
    languageDao: LanguageDao;
    postDao: PostDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.postDao = new PostDao(this.db);
        this.pageDao = new PageDao(this.db);
        this.languageDao = new LanguageDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'langIds',
            'layoutPageId',
            'title',
        ]);

        const now: number = Date.now();

        const post: PostEntity | null = this.getPostWithParams({
            createdTime: now,
            updatedTime: now
        });

        const isFindPost: PostEntity | null = await this.postDao.getItem({slug: post.slug});

        if (isFindPost) {

            throw new ErrorModel('Post Name Defined', 400);
        }

        await this.postDao.insert(new PostEntity({...post}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: PostEntity | null = await this.postDao.getItem({_id});

        if (!find) {

            this.notFoundError(new ErrorModel('Post not found'));

            return;
        }

        await this.postDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const post: PostEntity | null = await this.postDao.getItem({_id});

        if (post) {

            this.ok({post});
        } else {

            this.notFoundError(new ErrorModel('Post Not Found!'));
        }

    }

    async items(): Promise<void> {

        const filter: PageEntity = this.getPostWithParams();

        const posts: PostEntity[] | null = await this.postDao.getItems(filter);

        this.ok({posts});
    }

    async partialUpdate(): Promise<void> {

        this.update();
    }

    async update(): Promise<void> {

        const _id: string = this.getParam('id');

        const post: PostEntity | null = this.getPostWithParams({
            updatedTime: Date.now()
        });

        const find: PostEntity | null = await this.postDao.getItem({_id});

        if (!find) {

            this.notFoundError(new ErrorModel('Post not found'));

            return;
        }

        const updatePost: PostEntity = new PostEntity({
            ...find,
            ...post,
            _id,
        });

        await this.postDao.updateWithId(_id, updatePost);

        this.ok({success: 'OK'});
    }

    getPostWithParams(values: any = {}): PostEntity {

        return this
            .setValues(values)
            .setValuesWithParams([
                'langIds',
                'layoutPageId',
                'slug',
                'title',
                'subtitle',
                'htmlCode',
                'keywords'
            ])
            .getValues();
    }
}
