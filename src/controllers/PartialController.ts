/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {Config, ErrorModel, HttpController, IBlueprintRoutes, mkdir, pathResolve, read, rootPathResolve, unlink, write} from 'hk-serv';

import {PartialDao} from '../dao/PartialDao';
import {PartialEntity} from '../entity/PartialEntity';

const viewsPath: string = Config.get('server.viewsFolder');

export class PartialController extends HttpController implements IBlueprintRoutes {

    partialDao: PartialDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.partialDao = new PartialDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'partialName',
            'code',
        ]);

        const partial: PartialEntity = this.getPartialWithParams();

        const find: PartialEntity | null = await this.partialDao.getItem({partialName: partial.partialName});

        if (find) {

            throw new ErrorModel('Partial Defined', 400);
        }

        await this.partialDao.insert(new PartialEntity({...partial}));

        await this.writePartial(partial);

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: PartialEntity | null = await this.partialDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Partial not found', 404);
        }

        this.unlinkPartial(find);

        await this.partialDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {

        const _id: string = this.getParam('id');

        const partial: PartialEntity | null = await this.partialDao.getItem({_id});

        if (partial) {

            partial.code = await this.getPartialCode(partial);

            this.ok({partial});
        } else {

            this.notFoundError(new ErrorModel('Partial Not Found!'));
        }

    }

    async items(): Promise<void> {

        const filter: PartialEntity = this.getPartialWithParams();

        const partials: PartialEntity[] | null = await this.partialDao.getItems(filter);

        this.ok({partials});
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const partial: PartialEntity = this.getPartialWithParams();
        const find: PartialEntity | null = await this.partialDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Partial not found', 404);
        }

        if (partial.partialName !== find.partialName) {

            partial.code = partial.code ? partial.code : (await this.getPartialCode(find));

            await this.unlinkPartial(find);

        }

        await this.writePartial(partial);

        await this.partialDao.updateWithId(_id, new PartialEntity({
            ...partial,
            _id
        }));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        this.update();
    }

    private getPartialWithParams(values: any = {}): PartialEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'partialName',
                'code',
            ])
            .getValues();
    }

    private async getPartialPath(partialName: string | undefined): Promise<string> {

        const folder: string = rootPathResolve(viewsPath, 'partials');

        await mkdir(folder);

        return pathResolve(viewsPath, 'partials', `${partialName}.ejs`);
    }

    private async getPartialCode(partial: PartialEntity): Promise<string> {

        const partialFile: string = await this.getPartialPath(partial.partialName);

        const buffer: Buffer = await read(partialFile);

        return buffer.toString();
    }

    private async writePartial(partial: PartialEntity): Promise<void> {

        const partialFile: string = await this.getPartialPath(partial.partialName);

        await write(partialFile, partial.code || '');

    }

    private async unlinkPartial(partial: PartialEntity): Promise<void> {

        const partialFile: string = await this.getPartialPath(partial.partialName);

        await unlink(partialFile);

    }
}
