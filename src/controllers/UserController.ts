/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';

import {UserDao} from '../dao/UserDao';
import {UserEntity} from '../entity/UserEntity';

export class UserController extends HttpController implements IBlueprintRoutes {

    userDao: UserDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.userDao = new UserDao(this.db);
    }

    async create(): Promise<void> {

        const user: UserEntity = this
            .setValues()
            .setValuesWithParams(['username', 'password'])
            .getValues();

        const find: UserEntity | null = await this.userDao.getItem({username: user.username});

        if (find) {

            throw new ErrorModel('User Defined', 400);
        }

        await this.userDao.insert(new UserEntity({...user}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: UserEntity | null = await this.userDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('User not found', 400);
        }

        await this.userDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const user: UserEntity | null = await this.userDao.getItem({_id});

        if (user) {

            this.ok({user});
        } else {

            this.notFoundError({message: 'User Not Found!'});
        }

    }

    async items(): Promise<void> {

        const users: UserEntity[] | null = await this.userDao.getItems({});

        this.ok({users});
    }

    async update(): Promise<void> {

        const _id: string = this.getParam('id');

        const find: UserEntity | null = await this.userDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('User not found', 400);
        }

        const user: UserEntity = this
            .setValues(find)
            .setValuesWithParams(['username', 'password'])
            .setValuesWithParams(['roles'], 'root')
            .getValues();

        await this.userDao.updateWithId(_id, new UserEntity(user));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        throw new Error('Method not implemented.');
    }

    async auth(): Promise<void> {
        this.ok({success: 'OK'});
    }

    async login(): Promise<void> {

        this.requireParameters(['username', 'password']);

        const user: UserEntity = this
            .setValues()
            .setValuesWithParams(['username', 'password'])
            .getValues();

        const find: UserEntity | null = await this.userDao.getItem(user);

        if (find) {

            this.setSession('login', find._id);

            this.ok({user: new UserEntity(find, true)});
        } else {

            this.notFoundError({message: 'User Not Found!'});
        }

    }

    async logout(): Promise<void> {

        const _id: string = this.getSession('login');

        const find: UserEntity | null = await this.userDao.getItem({_id});

        if (find) {

            this.setSession('login', false);

            this.ok({success: 'OK'});

        } else {

            this.notFoundError({message: 'User Not Found!'});
        }

    }
}
