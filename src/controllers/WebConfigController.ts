/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';

import {WebConfigDao} from '../dao/WebConfigDao';
import {WebConfigEntity} from '../entity/WebConfigEntity';

export class WebConfigController extends HttpController implements IBlueprintRoutes {

    webConfigDao: WebConfigDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.webConfigDao = new WebConfigDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'langId',
            'title',
            'metadata',
        ]);

        const webConfig: WebConfigEntity = this.getWebConfigWithParams();

        const find: WebConfigEntity | null = await this.webConfigDao.getItem({title: webConfig.title});

        if (find) {

            throw new ErrorModel('WebConfig Defined', 400);
        }

        await this.webConfigDao.insert(new WebConfigEntity({...webConfig}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: WebConfigEntity | null = await this.webConfigDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('WebConfig not found', 404);
        }

        await this.webConfigDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const webConfig: WebConfigEntity | null = await this.webConfigDao.getItem({_id});

        if (webConfig) {

            this.ok({webConfig});
        } else {

            this.notFoundError({message: 'WebConfig Not Found!'});
        }

    }

    async items(): Promise<void> {

        const webConfigs: WebConfigEntity[] | null = await this.webConfigDao.getItems({});

        this.ok({webConfigs});
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const webConfig: WebConfigEntity = this.getWebConfigWithParams({
            updatedTime: Date.now()
        });

        await this.webConfigDao.updateWithId(_id, new WebConfigEntity({
            ...webConfig,
            _id
        }));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        const _id: string = this.getParam('id');

        const webConfig: WebConfigEntity = this.getWebConfigWithParams();

        const find: WebConfigEntity | null = await this.webConfigDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Content not found!', 404);
        }

        await this.webConfigDao.updateWithId(_id, new WebConfigEntity({
            ...webConfig,
            _id
        }));

        this.ok({success: 'OK'});
    }

    private getWebConfigWithParams(values: any = {}): WebConfigEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'langId',
                'title',
                'metadata',
            ])
            .getValues();
    }
}
