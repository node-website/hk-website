/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {Config, HttpController, rootPathResolve} from 'hk-serv';

const singlePage: string = Config.get('server.singlePage');
const publicFolder: string = Config.get('server.publicFolder');

export class SinglePageController extends HttpController {

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

    }

    public async index(): Promise<void> {

        const file: string = rootPathResolve(publicFolder, singlePage);

        this.response.sendFile(file);
    }
}
