/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import {ErrorModel, HttpController, IBlueprintRoutes} from 'hk-serv';

import {LanguageDao} from '../dao/LanguageDao';
import {LanguageEntity} from '../entity/LanguageEntity';

export class LanguageController extends HttpController implements IBlueprintRoutes {

    languageDao: LanguageDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.languageDao = new LanguageDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'code',
            'name',
        ]);

        const language: LanguageEntity = this.getLanguageWithParams();

        const find: LanguageEntity | null = await this.languageDao.getItem({code: language.code});

        if (find) {

            throw new ErrorModel('Language Defined', 400);
        }

        if (language.isDefault) {
            await this.languageDao.update({}, {isDefault: false});
        }

        await this.languageDao.insert(new LanguageEntity({...language}));

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: LanguageEntity | null = await this.languageDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Language not found', 404);
        }

        await this.languageDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {
        const _id: string = this.getParam('id');

        const language: LanguageEntity | null = await this.languageDao.getItem({_id});

        if (!language) {

            throw new ErrorModel('Language Not Found!', 404);
        }

        this.ok({language});
    }

    async items(): Promise<void> {

        const languages: LanguageEntity[] | null = await this.languageDao.getItems({});

        this.ok({languages});
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const language: LanguageEntity = this.getLanguageWithParams({
            updatedTime: Date.now()
        });

        const find: LanguageEntity | null = await this.languageDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Language not found!', 404);
        }

        if (language.isDefault) {
            await this.languageDao.update({}, {isDefault: false});
        }

        await this.languageDao.updateWithId(_id, new LanguageEntity({
            ...find,
            ...language,
            _id
        }));

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        await this.update();
    }

    private getLanguageWithParams(values: any = {}): LanguageEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'code',
                'name',
                'isDefault',
                'direction',
            ])
            .getValues();
    }
}
