/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import * as fs from 'fs';
import {Config, ErrorModel, HttpController, rootPathResolve} from 'hk-serv';
import * as _ from 'lodash';

import {ContentDao} from '../dao/ContentDao';
import {LanguageDao} from '../dao/LanguageDao';
import {PageDao} from '../dao/PageDao';
import {PostDao} from '../dao/PostDao';
import {ContentEntity} from '../entity/ContentEntity';
import {LanguageEntity} from '../entity/LanguageEntity';
import {PageEntity} from '../entity/PageEntity';
import {PostEntity} from '../entity/PostEntity';

const viewsPath: string = Config.get('server.viewsFolder');

export class ViewController extends HttpController {

    languageDao: LanguageDao;
    postDao: PostDao;
    contentDao: ContentDao;
    pageDao: PageDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.languageDao = new LanguageDao(this.db);
        this.postDao = new PostDao(this.db);
        this.contentDao = new ContentDao(this.db);
        this.pageDao = new PageDao(this.db);
    }

    public async viewEngine(): Promise<void> {

        // /dil/page-slug/content-slug
        const language: LanguageEntity = await this.getLanguage();
        const post: PostEntity | null = this.getPost(language);
        const page: PageEntity = await this.getPage(language, post);

        await this.showPage(language, post, page);
    }

    private async getLanguage(): Promise<LanguageEntity> {

        const langCode: string = this.getParam('l') || this.getParam('langOrPageOrPost');

        const _current: LanguageEntity | null = await this.languageDao.getItem({code: langCode});

        if (_current) {
            return _current;
        }

        const _default: LanguageEntity | null = await this.languageDao.getItem({isDefault: true});

        return _default || new LanguageEntity({code: 'tr', name: 'TR'});
    }

    private getMenu(lang: LanguageEntity, parentId?: string): PageEntity[] {
        const result: PageEntity[] | null = this
            .pageDao
            .getItems((page: PageEntity) => {

                const isParent: boolean = parentId ? page.parentId === parentId : true;
                const isLang: boolean = (page.langIds || []).includes(lang._id || '');

                return isParent && isLang && page.shownMenu === true;
            });

        const sorted: PageEntity[] = _.sortBy(result, 'order');

        for (const item of sorted) {

            item.children = this.getMenu(lang, item._id);
        }

        return sorted;
    }

    private getContents(page: PageEntity): ContentEntity[] {

        const contents: ContentEntity[] | null = this
            .contentDao
            .getItems((content: ContentEntity) => _.includes(page.contentIds, content._id));

        return _.orderBy(contents, ['createdTime'], ['desc']) || [];
    }

    private getData(page: PageEntity): any {

        const contents: ContentEntity[] | null = this.getContents(page);

        const postItems: any = _.reduce(page.postLayoutIds, (posts: any, layoutId: string) => {

            const layoutPage: PageEntity | null = this
                .pageDao
                .getItem({_id: layoutId});

            if (!(layoutPage && layoutPage.slug)) {
                return posts;
            }

            const result: PostEntity[] | null = this
                .postDao
                .getItems({layoutPageId: layoutId});

            const sorted: PostEntity[] = _.sortBy(result, 'order');

            return {
                ...posts,
                [layoutPage.slug]: this.getPaginatedItems(sorted),
            };
        }, {});

        return _.reduce(contents, (data: any, item: ContentEntity) => ({
            ...data,
            keywords: [
                ...(data.keywords || []),
                ...(item.keywords || []),
            ],
            [item.contentName]: item
        }), {
            ...postItems
        });
    }

    private getSlugWithParams(paramName: string): string | null {

        const slug: string = this.getParam(paramName);

        const name: string = _.chain(slug)
            .trim('/')
            .replace(/(\.html|\.htm|\.do|\.asp|\.php|\.aspx|\.jsf|\.jsp)/, '')
            .value();

        return _.isEmpty(name) ? null : name;
    }

    private getSlug(lang: LanguageEntity): string | null {
        // /en/details/post-2.html
        // /details/post-2.html?l=en
        // /post-2.html?l=en
        // /post-2.html
        // /en

        // if url path /:langOrPageOrPost
        const langOrPageOrPostSlug: string | null = this.getSlugWithParams('langOrPageOrPost');

        if (langOrPageOrPostSlug === lang.code) {
            return 'index';
        }

        // if url path /:l/:pageOrPostSlug
        const pageOrPostSlug: string | null = this.getSlugWithParams('pageOrPostSlug');

        // if url path /:l/:pageSlug/:postSlug
        const postSlug: string | null = this.getSlugWithParams('postSlug');
        const pageSlug: string | null = this.getSlugWithParams('pageSlug');

        return langOrPageOrPostSlug || pageOrPostSlug || postSlug || pageSlug || 'index';
    }

    private getPost(lang: LanguageEntity): PostEntity | null {

        const slug: string | null = this.getSlug(lang);

        if (!slug) {
            return null;
        }

        return this.postDao.getItem((post: PostEntity) => {
            return post.slug === slug && (post.langIds || []).includes(lang._id || '');
        });
    }

    private async getPage(lang: LanguageEntity, post: PostEntity | null): Promise<PageEntity> {

        const slug: string | null = this.getSlug(lang);

        const currentPage: PageEntity | null = this.pageDao.getItem((page: PageEntity) => {

            const isLang: boolean = !!lang._id && (page.langIds || []).includes(lang._id);

            // post varsa post layout'a bakılır. Post yoksa page.slug kontrolü sağlanır.
            const isPostLayout: boolean = post ? (page._id === post.layoutPageId) : false;
            const isPageSlug: boolean = page.slug === slug;

            return isLang && (isPostLayout || isPageSlug);
        });

        if (currentPage) {

            return currentPage;
        }

        const page404: PageEntity | null = await this.pageDao.getItem((p: PageEntity) => p.slug === '404');

        if (page404) {

            return page404;
        }

        throw new ErrorModel('Page not found!', 404);
    }

    private getPaginatedItems(items: any[]): any {

        const page: number = this.getParam('page') || 1;
        const pageSize: number = this.getParam('pageSize') || 20;

        const offset: number = (page - 1) * pageSize;
        const pagedItems: any[] = _.drop(items, offset).slice(0, pageSize);

        return {
            page,
            pageSize,
            total: items.length,
            items: pagedItems,
            totalPages: Math.ceil(items.length / pageSize),
        };
    }

    private async showPage(language: LanguageEntity, post: PostEntity | null, page: PageEntity): Promise<void> {

        const menu: PageEntity[] = this.getMenu(language);

        const data: any = this.getData(page);

        const options: any = {
            menu,
            language,
            post,
            page,
            keywords: [
                ...(page && page.keywords) || [],
                ...(data && data.keywords) || [],
                ...(post && post.keywords) || [],
            ],
            title: post ? post.title : page.title,
            data: {
                post,
                ...data
            },
        };

        const view: string = rootPathResolve(viewsPath, `${page._id}.ejs`);

        if (page && page._id && fs.existsSync(view)) {

            this.view(page._id, {
                __all: options,
                ...options
            });

            return;
        }

        this.view('404', {
            __all: options,
            ...options
        });
    }
}
