/**
 * Created by hakan on 05/10/2019.
 */
import {NextFunction, Response} from 'express';
import fs from 'fs';
import {Config, ErrorModel, HttpController, IBlueprintRoutes, mkdir, pathResolve, read, rootPathResolve, unlink, write} from 'hk-serv';
import _ from 'lodash';

import {LanguageDao} from '../dao/LanguageDao';
import {PageDao} from '../dao/PageDao';
import {PageEntity} from '../entity/PageEntity';

const viewsPath: string = Config.get('server.viewsFolder');

export class PageController extends HttpController implements IBlueprintRoutes {

    pageDao: PageDao;
    languageDao: LanguageDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.pageDao = new PageDao(this.db);
        this.languageDao = new LanguageDao(this.db);
    }

    async create(): Promise<void> {

        this.requireParameters([
            'langIds',
            'title',
            'slug',
            'code'
        ]);

        const page: PageEntity = this.getPageWithParams();

        const isFindPage: PageEntity | null = await this.pageDao.getItem((p: PageEntity) => {

            for (let i: number = 0; i < (p.langIds || []).length; i++) {
                const langId: string | undefined = p.langIds && p.langIds[i];

                if (_.includes(page.langIds, langId)) {
                    return p.slug === page.slug;
                }
            }

            return false;
        });

        if (isFindPage) {

            throw new ErrorModel('Page Defined', 400);
        }

        const newPage: PageEntity = new PageEntity({...page});

        await this.pageDao.insert(newPage);

        await this.writePage({
            ...newPage,
            code: page.code,
        });

        this.ok({success: 'OK'});
    }

    async destroy(): Promise<void> {
        const _id: string = this.getParam('id');

        const find: PageEntity | null = await this.pageDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Page not found!', 404);
        }

        this.unlinkPage(find);

        await this.pageDao.deleteWithId(_id);

        this.ok({success: 'OK'});
    }

    async item(): Promise<void> {

        const _id: string = this.getParam('id');

        const page: PageEntity | null = await this.pageDao.getItem({_id});

        if (page) {

            page.code = await this.readPageCode(page);

            this.ok({page});
        } else {

            this.notFoundError(new ErrorModel('Page Not Found!'));
        }
    }

    async items(): Promise<void> {

        const isFlat: boolean = this.getParam('flat');

        const filter: PageEntity = this.getPageWithParams();

        if (isFlat) {

            const pages: PageEntity[] | null = await this.pageDao.getItems(filter);

            this.ok({pages});
        } else {

            const pages: PageEntity[] = await this.getRecursivePages();

            this.ok({pages});
        }
    }

    async update(): Promise<void> {
        const _id: string = this.getParam('id');

        const page: PageEntity = this.getPageWithParams({
            updatedTime: Date.now()
        });

        const find: PageEntity | null = await this.pageDao.getItem({_id});

        if (!find) {

            throw new ErrorModel('Page not found!', 404);
        }

        const updatePage: PageEntity = new PageEntity({
            ...find,
            ...page,
            _id,
        });

        await this.writePage({
            ...updatePage,
            code: page.code,
        });

        await this.pageDao.updateWithId(_id, updatePage);

        this.ok({success: 'OK'});
    }

    async partialUpdate(): Promise<void> {
        this.update();
    }

    private async getRecursivePages(parentId?: string): Promise<PageEntity[]> {
        const result: PageEntity[] | null = await this.pageDao.getItems({parentId: parentId ? parentId : null});

        if (!result) {
            return [];
        }

        const sorted: PageEntity[] = _.sortBy<PageEntity>(result, 'order');

        for (const item of sorted) {
            item.children = await this.getRecursivePages(item._id);
        }

        return sorted;
    }

    private getPageWithParams(values: any = {}): PageEntity {
        return this
            .setValues(values)
            .setValuesWithParams([
                'langIds',
                'slug',
                'title',
                'order',
                'shownMenu',
                'parentId',
                'keywords',
                'postLayoutIds',
                'contentIds',
                'code',
            ])
            .getValues();
    }

    private async getPagePath(page: PageEntity): Promise<string> {

        const folder: string = rootPathResolve(viewsPath);

        await mkdir(folder);

        return pathResolve(folder, `${page._id}.ejs`);
    }

    private async readPageCode(page: PageEntity): Promise<string> {

        const pageFile: string = await this.getPagePath(page);

        if (!fs.existsSync(pageFile)) {

            throw new ErrorModel('View file not found!', 404);
        }

        const buffer: Buffer = await read(pageFile);

        return buffer
            .toString()
            .replace(
                /<%- include\('partials\/(.+\.ejs(\s|))'\) -%>/g,
                (match: string, group: string) => `<%- ${group.trim()} -%>`
            );
    }

    private async writePage(page: PageEntity): Promise<void> {

        const pageFile: string = await this.getPagePath(page);
        const code: string = (page.code || '').replace(
            /<%-((\s|).+(\s|))-%>/g,
            (match: string, group: string) => `<%- include('partials/${group.trim()}') -%>`
        );

        await write(pageFile, code);

    }

    private async unlinkPage(page: PageEntity): Promise<void> {

        const pageFile: string = await this.getPagePath(page);

        if (fs.existsSync(pageFile)) {

            await unlink(pageFile);
        }

    }

}
