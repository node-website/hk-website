/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes} from 'hk-serv';

import {PageController} from '../controllers/PageController';
import {auth} from '../middleware/auth';

export class PageRoute {

    public static create(): any[] {

        const authMiddleware: () => void = auth();

        return CoreRoutes.blueprintRoutes(PageController, '/api/pages', {
            create: [authMiddleware],
            destroy: [authMiddleware],
            update: [authMiddleware]
        });
    }

}
