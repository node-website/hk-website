/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes} from 'hk-serv';

import {StyleController} from '../controllers/StyleController';
import {auth} from '../middleware/auth';

export class StyleRoute {

    public static create(): any[] {

        const authMiddleware: () => void = auth();

        return CoreRoutes.blueprintRoutes(StyleController, '/api/styles', {
            create: [authMiddleware],
            destroy: [authMiddleware],
            update: [authMiddleware]
        });
    }

}
