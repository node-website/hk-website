/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes} from 'hk-serv';

import {WebConfigController} from '../controllers/WebConfigController';
import {auth} from '../middleware/auth';

export class WebConfigRoute {

    public static create(): any[] {

        const authMiddleware: () => void = auth();

        return CoreRoutes.blueprintRoutes(WebConfigController, '/api/web-configs', {
            create: [authMiddleware],
            destroy: [authMiddleware],
            update: [authMiddleware]
        });
    }

}
