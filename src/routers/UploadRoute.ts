/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes, Method} from 'hk-serv';

import {UploadController} from '../controllers/UploadController';

export class UploadRoute {

    public static create(): any[] {

        return CoreRoutes.routes(
            UploadController,
            {method: Method.post, routePath: '/api/upload', fnName: 'upload'},
        );

    }

}
