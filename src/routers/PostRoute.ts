/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes} from 'hk-serv';

import {PostController} from '../controllers/PostController';
import {auth} from '../middleware/auth';

export class PostRoute {

    public static create(): any[] {

        const authMiddleware: () => void = auth();

        return CoreRoutes.blueprintRoutes(PostController, '/api/posts', {
            create: [authMiddleware],
            destroy: [authMiddleware],
            update: [authMiddleware]
        });
    }

}
