/**
 * Created by hakan on 04/07/2017.
 */

import {CoreRoutes, Method} from 'hk-serv';

import {UserController} from '../controllers/UserController';
import {auth} from '../middleware/auth';

export class UserRoute {

    public static create(): any {

        const authMiddleware: () => void = auth();

        return [
            // user blueprint
            CoreRoutes.blueprintRoutes(UserController, '/api/users', {
                all: [authMiddleware]
            }),

            // custom routes
            CoreRoutes.routes(
                UserController,
                {method: Method.post, routePath: '/api/login', fnName: 'login'},
                {method: Method.post, routePath: '/api/auth', fnName: 'auth', middleware: [authMiddleware]},
                {method: Method.post, routePath: '/api/logout', fnName: 'logout', middleware: [authMiddleware]}
            )
        ];
    }

}
