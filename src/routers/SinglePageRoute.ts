/**
 * Created by hakan on 04/07/2017.
 */
import {Config, CoreRoutes, Method} from 'hk-serv';
import {dirname} from 'path';

import {SinglePageController} from '../controllers/SinglePageController';

const singlePage: string = Config.get('server.singlePage');

export class SinglePageRoute {

    public static create(): any[] {
        const folder: string = dirname(singlePage.trim());
        return [
            CoreRoutes.route(SinglePageController, {
                    method: Method.get,
                    routePath: new RegExp(`^${folder}.+`),
                    fnName: 'index'
                },
            )
        ];
    }

}
