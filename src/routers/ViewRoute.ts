/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes, Method} from 'hk-serv';

import {ViewController} from '../controllers/ViewController';

export class ViewRoute {

    public static create(): any[] {

        // /en/details/post-2.html
        // /details/post-2.html?l=en
        // /post-2.html?l=en
        // /post-2.html
        // /en
        return CoreRoutes.routes(
            ViewController,
            {method: Method.get, routePath: '/:l/:pageSlug/:postSlug', fnName: 'viewEngine'},
            {method: Method.get, routePath: '/:l/:pageOrPostSlug', fnName: 'viewEngine'},
            {method: Method.get, routePath: '/:langOrPageOrPost?', fnName: 'viewEngine'},
        );

    }

}
