/**
 * Created by hakan on 04/07/2017.
 */
import {CoreRoutes} from 'hk-serv';

import {TranslateController} from '../controllers/TranslateController';
import {auth} from '../middleware/auth';

export class TranslateRoute {

    public static create(): any[] {

        const authMiddleware: () => void = auth();

        return CoreRoutes.blueprintRoutes(TranslateController, '/api/translates', {
            create: [authMiddleware],
            destroy: [authMiddleware],
            update: [authMiddleware]
        });
    }

}
