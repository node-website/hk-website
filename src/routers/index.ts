/**
 * Created by hakan on 04/07/2017.
 */

import {ContentRoute} from './ContentRoute';
import {LanguageRoute} from './LanguageRoute';
import {PageRoute} from './PageRoute';
import {PartialRoute} from './PartialRoute';
import {PostRoute} from './PostRoute';
import {SinglePageRoute} from './SinglePageRoute';
import {StyleRoute} from './StyleRoute';
import {TranslateRoute} from './TranslateRoute';
import {UploadRoute} from './UploadRoute';
import {UserRoute} from './UserRoute';
import {ViewRoute} from './ViewRoute';
import {WebConfigRoute} from './WebConfigRoute';

export default [
    LanguageRoute.create(),
    TranslateRoute.create(),
    WebConfigRoute.create(),
    PartialRoute.create(),
    PageRoute.create(),
    ContentRoute.create(),
    PostRoute.create(),
    UploadRoute.create(),
    UserRoute.create(),
    StyleRoute.create(),
    SinglePageRoute.create(),
    ViewRoute.create()
];
