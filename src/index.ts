/**
 * Created by hakan on 04/07/2017.
 */
import {ApplicationServer, Config, ErrorModel, LowDB} from 'hk-serv';

import Routers from './routers';
import './styles';

const dbConf: string = Config.get('lowDB');

class App extends ApplicationServer {

    async injectRequest(): Promise<{ [key: string]: any }> {
        const injects: { [key: string]: any } = {};

        try {
            injects.db = await LowDB.connect(dbConf);

            injects.db
                .defaults({
                    users: [
                        {
                            _id: 'e00804b0-e7cb-11e9-bc8c-7d34405a3b8e',
                            username: 'root',
                            password: 'hkn',
                            roles: ['root']
                        },
                        {
                            _id: 'e00804b0-e7cb-11e9-bc8c-7d34405a3b8e',
                            username: 'admin',
                            password: 'admin',
                            roles: ['admin']
                        }
                    ],
                    languages: [],
                    translates: [],
                    webConfig: {},
                    pages: [],
                    partials: [],
                    styles: [],
                    forms: [],
                    contents: [],
                    posts: [],
                })
                .write();

        } catch (e) {

            throw new ErrorModel('DATABASE_CONNECTION_ERROR', 200, e);
        }

        return injects;
    }

    async registerRoutes(): Promise<any[]> {
        return Routers;
    }
}

(async (): Promise<void> => {

    const app: App = new App();
    await app.listen();

})();
