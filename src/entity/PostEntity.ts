import * as uuid from 'uuid';

export class PostEntity {

    _id?: string;
    langIds?: string[];
    layoutPageId?: string;
    slug?: string;
    title: string;
    subtitle?: string;
    htmlCode: string;
    keywords?: string[];
    createdTime?: number;
    updatedTime?: number;

    public constructor(item: PostEntity) {
        this._id = item._id ? item._id : uuid.v1();

        this.langIds = item.langIds;
        this.layoutPageId = item.layoutPageId;
        this.slug = item.slug;
        this.title = item.title;
        this.subtitle = item.subtitle;
        this.htmlCode = item.htmlCode;
        this.keywords = item.keywords || [];
        this.createdTime = item.createdTime;
        this.updatedTime = item.updatedTime || Date.now();

    }

}
