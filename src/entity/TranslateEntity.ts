import * as uuid from 'uuid';

export class TranslateEntity {

    _id?: string;
    langId?: string;
    key?: string;
    value?: string;

    public constructor(item: TranslateEntity) {
        this._id = item._id ? item._id : uuid.v1();
        this.langId = item.langId;
        this.key = item.key;
        this.value = item.value;
    }

}
