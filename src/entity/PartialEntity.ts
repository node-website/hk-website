import * as uuid from 'uuid';

export class PartialEntity {

    _id?: string;
    partialName?: string;
    code?: string;

    public constructor(item: PartialEntity, isSave: boolean = true) {
        this._id = item._id ? item._id : uuid.v1();
        this.partialName = item.partialName;

        if (isSave) {
            return;
        }

        this.code = item.code;
    }

}
