import * as uuid from 'uuid';

export class UserEntity {

    _id?: string;
    roles?: string[];
    username?: string;
    password?: string;

    public constructor(item: UserEntity, isPublic: boolean = false) {

        this._id = item._id ? item._id : uuid.v1();
        this.username = item.username;
        this.roles = item.roles;

        if (isPublic) {
            return;
        }

        this.password = item.password;

    }

}
