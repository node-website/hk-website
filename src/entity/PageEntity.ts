import * as uuid from 'uuid';

export class PageEntity {

    _id?: string;
    langIds?: string[];
    title?: string;
    slug?: string;
    order?: number;
    shownMenu?: boolean;
    parentId?: string | null;
    keywords?: string[];
    postLayoutIds?: string[];
    contentIds?: string[];
    code?: string;
    children?: PageEntity[];

    public constructor(item: PageEntity, isSave: boolean = true) {
        this._id = item._id ? item._id : uuid.v1();
        this.title = item.title;
        this.langIds = item.langIds;
        this.slug = item.slug;
        this.order = item.order || 0;
        this.shownMenu = !!item.shownMenu;
        this.parentId = item.parentId || null;
        this.keywords = item.keywords || [];
        this.postLayoutIds = item.postLayoutIds || [];
        this.contentIds = item.contentIds || [];

        if (isSave) {
            return;
        }
        this.code = item.code;
        this.children = item.children || [];
    }

}
