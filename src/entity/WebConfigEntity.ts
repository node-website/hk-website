import * as uuid from 'uuid';

export class WebConfigEntity {

    _id?: string;
    langId?: string;
    title?: string;
    metadata?: string[][];

    public constructor(item: WebConfigEntity) {
        this._id = item._id ? item._id : uuid.v1();
        this.langId = item.langId;
        this.title = item.title;
        this.metadata = item.metadata || [];
    }

}
