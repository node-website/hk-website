import * as uuid from 'uuid';

export class StyleEntity {

    _id?: string;
    styleName?: string;
    code?: string;

    public constructor(item: StyleEntity, isSave: boolean = true) {
        this._id = item._id ? item._id : uuid.v1();
        this.styleName = item.styleName;

        if (isSave) {
            return;
        }

        this.code = item.code;
    }

}
