import * as uuid from 'uuid';

import {ImageEntity} from './ImageEntity';

export class ContentEntity {

    _id?: string;
    contentName: string;
    title: string;
    subtitle?: string;
    images: ImageEntity[];
    htmlCode: string;
    json: any;
    keywords?: string[];
    subContents?: ContentEntity[];
    createdTime?: number;
    updatedTime?: number;

    public constructor(item: ContentEntity, isSave: boolean = true) {
        this._id = item._id ? item._id : uuid.v1();

        this.contentName = item.contentName;
        this.title = item.title;
        this.subtitle = item.subtitle;
        this.images = item.images || [];
        this.htmlCode = item.htmlCode;
        this.json = item.json || [];
        this.keywords = item.keywords || [];
        this.createdTime = item.createdTime;
        this.updatedTime = item.updatedTime || Date.now();

        if (isSave) {
            return;
        }
        this.subContents = item.subContents || [];
    }

}
