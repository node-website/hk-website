export class ImageEntity {

    originalname?: string;
    filename?: string;
    mimetype?: string;
    size?: number;
    createdTime?: Date;

    public constructor(item: ImageEntity) {
        this.originalname = item.originalname;
        this.filename = item.filename;
        this.mimetype = item.mimetype;
        this.size = item.size || 0;
        this.createdTime = item.createdTime;
    }

}
