import * as uuid from 'uuid';

export class LanguageEntity {

    _id?: string;
    code?: string;
    name?: string;
    isDefault?: boolean;
    direction?: 'ltr' | 'rtl';

    public constructor(item: LanguageEntity) {
        this._id = item._id ? item._id : uuid.v1();
        this.code = item.code;
        this.name = item.name;
        this.isDefault = item.isDefault || false;
        this.direction = item.direction || 'ltr';
    }

}
