import {LowDbCoreDao} from 'hk-serv';

import {StyleEntity} from '../entity/StyleEntity';

export class StyleDao extends LowDbCoreDao<StyleEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'styles');
    }
}
