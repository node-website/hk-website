import {LowDbCoreDao} from 'hk-serv';

import {PartialEntity} from '../entity/PartialEntity';

export class PartialDao extends LowDbCoreDao<PartialEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'partials');
    }
}
