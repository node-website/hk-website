import {LowDbCoreDao} from 'hk-serv';

import {ContentEntity} from '../entity/ContentEntity';

export class ContentDao extends LowDbCoreDao<ContentEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'contents');
    }
}
