import {LowDbCoreDao} from 'hk-serv';

import {LanguageEntity} from '../entity/LanguageEntity';

export class LanguageDao extends LowDbCoreDao<LanguageEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'languages');
    }
}
