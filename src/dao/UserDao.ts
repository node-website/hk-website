import {LowDbCoreDao} from 'hk-serv';

import {UserEntity} from '../entity/UserEntity';

export class UserDao extends LowDbCoreDao<UserEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'users');
    }
}
