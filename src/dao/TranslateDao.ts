import {LowDbCoreDao} from 'hk-serv';

import {TranslateEntity} from '../entity/TranslateEntity';

export class TranslateDao extends LowDbCoreDao<TranslateEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'translates');
    }
}
