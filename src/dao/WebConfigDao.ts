import {LowDbCoreDao} from 'hk-serv';

import {WebConfigEntity} from '../entity/WebConfigEntity';

export class WebConfigDao extends LowDbCoreDao<WebConfigEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'webConfig');
    }
}
