import {LowDbCoreDao} from 'hk-serv';

import {PageEntity} from '../entity/PageEntity';

export class PageDao extends LowDbCoreDao<PageEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'pages');
    }
}
