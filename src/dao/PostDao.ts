import {LowDbCoreDao} from 'hk-serv';

import {PostEntity} from '../entity/PostEntity';

export class PostDao extends LowDbCoreDao<PostEntity> {

    protected db: any;

    public constructor(db: any) {
        super(db, 'posts');
    }
}
