const fs = require("fs");
const path = require("path");
const util = require("util");

const unlinkAsync = util.promisify(fs.unlink);
const symlinkAsync = util.promisify(fs.symlink);
const readFileAsync = util.promisify(fs.readFile);
const writeFileAsync = util.promisify(fs.writeFile);

const unlink = async (file) => {
    return unlinkAsync(file);
};

const read = async (file) => {
    return readFileAsync(file);
};

const write = async (file, text) => {
    return writeFileAsync(file, text);
};

const isSymlink = (filepath) => {
    if (typeof filepath !== "string") {
        throw new TypeError("expected filepath to be a string");
    }
    try {
        let stats = fs.lstatSync(path.resolve(filepath));
        return stats.isSymbolicLink();
    }
    catch (err) {
        if (err.code === "ENOENT") {
            return false;
        }
        throw err;
    }
};

/**
 * örnek olması adına burda dursun.
 * @param filePath
 * @param symlinkPath
 */
const createSymlink = (filePath, symlinkPath) => {
    return symlinkAsync(filePath, symlinkPath);
};

/**
 * Remove directory recursively
 * @param {string} dir_path
 * @see https://stackoverflow.com/a/42505874/3027390
 */
function rimraf(dir_path) {
    if (fs.existsSync(dir_path)) {
        fs.readdirSync(dir_path).forEach((entry) => {

            const entry_path = path.join(dir_path, entry);

            if (fs.lstatSync(entry_path).isDirectory()) {
                rimraf(entry_path);
            }
            else {
                fs.unlinkSync(entry_path);
            }
        });
        fs.rmdirSync(dir_path);
    }
}

module.exports = {unlink, read, write, isSymlink, createSymlink, rimraf};
