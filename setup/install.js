const {website} = require("../package");
const {unlink, read, write, createSymlink} = require("./fs-utils");
const fs = require('fs');
const path = require('path');
const shell = require('shelljs');

const {DOMAIN, INDEX, PORT, MAX_BODY_SIZE} = website;

const confPaths = {
    available: path.join(`/etc/nginx/sites-available/${DOMAIN[0]}.conf`),
    enabled: path.join(`/etc/nginx/sites-enabled/${DOMAIN[0]}.conf`)
};

const shells = {
    nginxReload: 'nginx -t && nginx -s reload',
    certbot: `certbot --nginx -d ${DOMAIN.join(' -d ')}`
};

function shellExec(successLog, sh) {
    const result = shell.exec(sh);
    if (result.code === 0) {
        if (successLog) {
            console.log(successLog);
        }
    }
    else {
        console.log(result.stderr)
    }
}

(async () => {

    const nginxConfig = await read("./site-config.conf");

    const websiteConfig = nginxConfig
        .toString()
        .replace(/{{DOMAIN}}/g, DOMAIN.join(' '))
        .replace(/{{INDEX}}/g, INDEX)
        .replace(/{{MAX_BODY_SIZE}}/g, MAX_BODY_SIZE || '10M')
        .replace(/{{PORT}}/g, PORT.toString());

    await write(confPaths.available, websiteConfig);

    if (fs.existsSync(confPaths.enabled)) {
        await unlink(confPaths.enabled);
    }

    await createSymlink(confPaths.available, confPaths.enabled);

    shellExec("NGINX reload.", shells.nginxReload);
    shellExec("SSL created.", shells.certbot);

    // add cron :)
    // crontab -l -> cronları listeler.
    // 0 12 * * * /usr/bin/certbot renew --quiet

})().catch(console.log);
