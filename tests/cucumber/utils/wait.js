
module.exports = async (t) => new Promise(resolve => setTimeout(resolve, t * 1000));
