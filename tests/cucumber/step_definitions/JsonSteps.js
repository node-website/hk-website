const {Given, When, Then} = require('cucumber');
const wait = require('../utils/wait');

Given(/^Kullanıcı adı ve parola ile giriş parametrelerini hazırla\. Kullanıcı adı: "([^"]*)" parola: "([^"]*)"$/, async function(username, password) {

  this.data = {username, password};

});

When(/^"([^"]*)" adresinden giriş yapmayı dene \("([^"]*)"\)$/, async function(endpoint, status) {

  console.log(`calling ${endpoint} endpoint...`);

  await wait(2);

  this.response = {
    url: `http://${endpoint}`,
    logon: status === "success"
  }

});
When(/^Giriş başarılı mı\?$/, function() {

  console.log("giriş başarılı mı?: ", !!this.response.logon);

  if (!this.response.logon) {
    throw 'Giriş başarısız.'
  }

});

Then(/^Giriş başarılı değil mi\?$/, function() {
  console.log("Giriş başarılı değil mi?: ", !!this.response.logon);
});

Then(/^Bitir\.$/, function() {

  console.log("test bitti :)")
});
