const axios = require("axios");
const {API_ENDPOINT, HTTP} = require("../envirmoment");

const axiosRequest = axios.create({
  withCredentials: true,
});

axiosRequest.interceptors.response.use((res) => res, (error) => {
  // Do something with response error
  let {response} = error;
  // response?.status === 401 es7 way.
  if (response && response.status === 401) {
    // Do logout;
    // Redirect to login;
    return Promise.reject(error.response);
  }
  else if (error.response) {
    // Do something.
    console.log('some API_CLIENT error');
    return Promise.reject(error.response);
  }
  return Promise.reject(error);
});

const request = async (method, service, payload = {}, headers = {}, {isUpload, options} = {}) => {
  try {

    const {data} = await axiosRequest({
      url: `${API_ENDPOINT}${service}`,
      data: payload,
      method,
      headers: {
        'Content-Type': !!isUpload ? 'multipart/form-data' : 'application/json',
        ...headers,
      },
      ...options
    });

    return data;

  }
  catch (error) {

    console.log("error: ", error);

    throw error;
  }

};

class Api {

  async get(service, payload = {}, headers = {}, options = {}) {
    return request(HTTP.GET, service, payload, headers, options);
  }

  async post(service, payload = {}, headers = {}, options = {}) {
    return request(HTTP.POST, service, payload, headers, options)
  }

  // async put(service, payload = {}, headers = {}, options = {}) {
  //     return request(HTTP.PUT, service, payload, headers, options);
  // }

  async patch(service, payload = {}, headers = {}, options = {}) {
    return request(HTTP.PATCH, service, payload, headers, options);
  }

  async delete(service, payload = {}, headers = {}, options = {}) {
    return request(HTTP.DELETE, service, payload, headers, options);
  }

}

// Make it singleton.
module.exports = new Api();
