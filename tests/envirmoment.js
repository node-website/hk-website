const isDev = process.env.NODE_ENV === "development";

const apiUrl = {
  development: "http://localhost:30000",
  production: ""
};

const HTTP = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  PATCH: "PATCH",
  DELETE: "DELETE"
};

const WEBSITE_ENDPOINT = isDev ? apiUrl.development : apiUrl.production;
const API_ENDPOINT = `${WEBSITE_ENDPOINT}/api`;

const APP_FOLDER = `${process.env.PUBLIC_URL}`;

module.exports = {
  HTTP,
  WEBSITE_ENDPOINT,
  API_ENDPOINT,
  APP_FOLDER
};
