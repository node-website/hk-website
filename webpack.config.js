const path = require('path');
const apidoc = require('apidoc');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = env => {
    const isDev = env !== 'production';
    return {
        entry: './src/index.ts',
        mode: isDev ? 'development' : 'production',
        output: {
            filename: 'index.js',
            path: path.resolve(__dirname, 'dist'),
        },
        resolve: {
            extensions: ['.ts', '.sass', '.scss', '.css']
        },
        module: {
            rules: [
                {
                    // Loads CSS into a file when you import it via Javascript
                    // Rules are set in MiniCssExtractPlugin
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                        'sass-loader'
                    ]
                },
                {
                    test: /.ts?$/,
                    use: [
                      {
                        loader: 'ts-loader',
                        options: {
                          allowTsInNodeModules: true
                        }
                      }
                    ]
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader',
                            options: {
                                minimize: true
                            }
                        }
                    ],
                },
            ]
        },
        target: 'node',
        externals: [
          nodeExternals({
            whitelist: ['hk-serv']
          }),
            (context, request, callback) => {
                if (/config\.json$/.test(request)) {
                    return callback(null, 'commonjs ./config.json');
                }
                callback();
            }
        ],
        plugins: [
            new webpack.EnvironmentPlugin({
                dev: isDev,
            }),
            new MiniCssExtractPlugin({
                filename: 'public/styles/main.bundle.css'
            }),
            new CopyWebpackPlugin([
                {
                    from: 'setup',
                    to: 'setup',
                    force: true
                },
                {
                    from: 'src/assets',
                    to: 'public',
                    force: true
                },
                {
                    from: 'src/views',
                    to: 'views',
                    force: true
                },
                {
                    from: 'src/config.json',
                    to: 'config.json',
                    transform(content) {
                        const sub = isDev ? 'development' : 'production';
                        const json = JSON.parse(content);
                        return JSON.stringify(json[sub] || json || '', null, 2);
                    },
                    force: true
                },
                {
                    from: 'package.json',
                    to: 'package.json',
                    transform(content) {

                        const json = JSON.parse(content.toString());
                        json.scripts = {
                            start: 'node index.js',
                            setup: 'node setup/install.js'
                        };

                        delete json.devDependencies;

                        return JSON.stringify(json, null, 2);
                    },
                    force: true
                },
            ]),
            () => {
                return apidoc.createDoc({
                    src: 'src',
                    dest: 'dist/public/docs',
                    silent: true
                });
            }
        ],
        watch: isDev
    };
};
